# Scénarii à mettre en place pour continuer à developper et tester le CDT pendant la phase d'exploitation GDF.

1. branche commune
    1. Booléen global + vérification d'un déposit
        * Le passage de GDF vers CDT se fait sans mise en prod ni script de migration des commandes militaires
        * Test constant de la stabilité du site sur deux environnement distincts
        * PPROD iso PROD
        * Machine spécifique pour la recette de CDT
        * Une seule branche commune -> permet de limiter les problèmes de commit / merge puisque DEV et LEGACY = emoovz_minarm
        * Faible surcharge de développement puisque les developpements sont séparés les uns des autres par le booléen.
    1. Environnements 
        * Deux env legacy avec base indépedante sur le même serveur : 
            * Un en mode CDT -> ex: port 8081
            * Un en mode GDF -> ex: Port par defaut   
        * Deux env de PPROD sur deux machines distinctes et base indépendante :
            * Un en mode CDT -> DG-WYCHEPROOF
            * Un en mode GDF -> PPROD actuelle (demeco-pp)  


1. Deux branches     
    1. Production sans séparation technique
        * Une branche bug-fix + évol gdf (emmovz-ged)
        * Une branche evol CDT (emoovz_minarm) 
        * Régulièrement faire des merges de emoovz_minarm vers emmovz-ged afin de garantir un code le plus identique possible
            * Attention risque de problème lié à un merge mal géré
            * temps de deploiement allongé par la necessité de faire ces merges manuels
        * Si service séparés : faire manuellement le merge de celui-ci
        * Prévoir des scripts de migrations lors du passage officiel en CDT
    1. Production avec séparation technique
        * Une branche bug-fix + évol gdf (emmovz-ged)
        * Une branche evol CDT (emoovz_minarm) 
        * Rigulièrement faire des merges de emoovz_minarm vers emmovz-ged afin de garantir un code le plus identique possible
             * Attention risque de problème lié à un merge mal géré
             * temps de deploiement allongé par la necessité de faire ces merges manuels
        * Faire deux fois le service pour GDF (emmovz-ged) en revanche le merge est en automatique sauf dans le cas où nous sommes en CDT
        * Prévoir des scripts de migrations lors du passage officiel en CDT
    1. Environnements 
        * Deux env legacy avec base indépedante et code basé sur des branches différentes sur le même serveur : 
            * Un en mode CDT -> ex: port 8081
            * Un en mode GDF -> ex: Port par defaut   
        * Deux env de PPROD sur deux machines distinctes et base indépendante et code basé sur des branches différentes :
            * Un en mode CDT -> DG-WYCHEPROOF
            * Un en mode GDF -> PPROD actuelle (demeco-pp)  


1. Mode altéré -> NOWAY !!!!!
    * 1 branche fusionnant emoovz-ged et emoovz_minarm
    * Comportement du CDT adapté au cas par cas pour rester iso GDF
    * Très grand surcoût en developpement pour adapter (de façon provisoire) une fonctionnalité CDT au GDF
    * Le fonctionnement à très peu de chance d'être ISO GDF
    * Les environnements sont identique à ceux d'aujourd'hui

> Techniquement la première solution semble la plus pertinente et la moins risquée.